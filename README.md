Install vundle package manager with(project github page: https://github.com/VundleVim/Vundle.vim) :

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

vim +PluginInstall +qall

Additional configuration:

CTags:

To generate tags for current project, run ctags command in root directory of
your project (--c++-kinds=+lpx adds also local variables, function prototypes
and external function support, this option is not recommended for big projects):

ctags -R --c++-kinds=+plx --fields=+afmikKlnsStz --extra=+q .

To generate ctags for external lib, just run commands like (In this example Qt and OpenCV):

ctags -R --sort=yes --c++-kinds=+plx --fields=+iaS --extra=+q --language-force=C++ -f qt5 <qt_path>

ctags -R --sort=yes --c++-kinds=+plx --fields=+iaS --extra=+q --language-force=C++ -f opencv <opencv_path>


Cscope: (source: http://cscope.sourceforge.net/large_projects.html)

find /project_directory_root -name '\*.cpp' -o -name '\*.h' > /desired_folder_for_cscopeDB/cscope.files

cscope -R -b (this will create cscope.out)

export CSCOPE_DB=/folder_containing/cscope.out (Can be added to .bashrc or whatever shell used)
