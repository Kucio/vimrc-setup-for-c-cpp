set nocompatible              " be iMproved, required

filetype off                  " required

set shell=/bin/bash

" project specific paths

set path+=~/<root-of-project>/**

" set the runtime path to include Vundle and initialize

set rtp+=~/.vim/bundle/Vundle.vim

" Tags generated with ctags

set tags+=<example-tag-file-location>/tag_file

"Automatically set current dir to edited file

cnoremap cd. lcd %:p:h

nnoremap ,cd :cd %:p:h<CR>:pwd<CR>

" Set CSCOPE_DB environmental variable to folder where command
" cscope was run
" How to cscope: http://cscope.sourceforge.net/large_projects.html

cs add $CSCOPE_DB

let g:syntastic_cpp_check_header = 1

let g:rehash256 = 1

:let g:NERDTreeWinSize=80

"cpp_settings

let g:cpp_class_scope_highlight=1

let g:cpp_concepts_highlight=1

let g:cpp_experimental_simple_template_highlight=1

call vundle#begin()

"cs to open cscope menu

nmap cs :!cd $CSCOPE_DB; cscope -d<CR>  

"F4 to debug

nmap <F4> :ConqueGdb<CR> 

"Ctrl-F4 to cscope any string search, replace text with desired string and
"press Enter

nmap <C-F4> :cs find t text

"Ctrl-F5 to cscope string search

nmap <C-F5> :cs find t <C-R><C-F><CR>

"Ctrl-F6 to cscope file search

nmap <C-F6> :cs find f <C-R><C-F><CR>

"Ctrl-F7 to cscope search

nmap <C-F7> :cs find s <C-R>=expand("<cword>")<CR><CR>

" F9 to open terminal

nmap <F9> :term<CR>

autocmd vimenter * NERDTree

autocmd vimenter * TagbarToggle

" tt to toggle nerdTree

nmap tt :NERDTreeToggle<CR>

" tb to open TagBar

nmap tb :TagbarToggle<CR>

"replace text

:nnoremap rr :%s/\<<C-r><C-w>\>//g<Left><Left>

"rn to rename local variable name

:nnoremap rn :%s/\<<C-r><C-w>\>/

"grep wrappers

:nnoremap gr :grep <cword> *<CR>

:nnoremap Gr :grep <cword> %:p:h/*<CR>

:nnoremap gR :grep '\b<cword>\b' *<CR>

:nnoremap GR :grep '\b<cword>\b' %:p:h/*<CR>

Plugin 'VundleVim/Vundle.vim'

Plugin 'mileszs/ack.vim'

Plugin 'apalmer1377/factorus'

Plugin 'multilobyte/gtags-cscope'

Plugin 'tomasr/molokai'

Plugin 'vim-airline/vim-airline'

Plugin 'vim-airline/vim-airline-themes'

Plugin 'octol/vim-cpp-enhanced-highlight'

Plugin 'tpope/vim-fugitive'

Plugin 'wincent/command-t'

Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

Plugin 'vim-syntastic/syntastic'

Plugin 'scrooloose/nerdtree'

Plugin 'Conque-GDB'

Plugin 'kien/ctrlp.vim'

Plugin 'majutsushi/tagbar'

Plugin 'universal-ctags/ctags'

Plugin 'flazz/vim-colorschemes'

Plugin 'vim-scripts/a.vim'

Plugin 'neoclide/coc.nvim'

Plugin 'neovim/nvim-lsp' " nvim-lsp

Plugin 'jackguo380/vim-lsp-cxx-highlight'

call vundle#end()            " required

filetype plugin indent on    " required

syntax on

colorscheme molokai

set backspace=indent,eol,start

syntax enable

set nu

"highlight matching brace

set showmatch

"highlight search result

set hlsearch

"highlight current cursor

set cursorline

"80 columns margin

set colorcolumn=80

"replace tab with spaces

set expandtab

set tabstop=4

"auto-indent

set shiftwidth=4

set autoindent
